/*What directive is used by Node.js in loading the modules it needs?
	-   ans. require()

What Node.js module contains a method for server creation?
	- ans. HTTP module
	
What is the method of the http object responsible for creating a server using Node.js?
	- ans. createServer()

What method of the response object allows us to set status codes and content types?
	- ans. writeHead()

Where will console.log() output its contents when run in Node.js?
	- ans. terminal

What property of the request object contains the address's endpoint?
	- ans. url

*/